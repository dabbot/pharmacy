import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import logo from './logo.svg';
import './App.css';
import ClientDetail from './components/ClientDetail/ClientDetail';
import Home from './components/pages/Home';

class App extends Component {

  render() {
    return (
      <Router>
        <div className="App">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <h1 className="App-title">Pharmacy</h1>
          </header>
          <div className="container">
            <Route exact path="/" component={Home} />
            <Route path="/clients/:client" component={ClientDetail} />
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
