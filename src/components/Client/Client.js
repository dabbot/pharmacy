import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Client extends Component {

  render() {
    return (
      <div className="client">
        {this.props.client ?
        <Link to={
          {
            pathname: `clients/${this.props.client.identifier}`,
            state: { client: this.props.client }
          }

        }>
          <div className="client-view">
            <div className="card client-card">
              <h3>{this.props.client.identifier}</h3>
              <span>{this.props.client.statusMessage}</span>
            </div>
          </div>
        </Link>
        : null }
      </div>
    );
  }
}

