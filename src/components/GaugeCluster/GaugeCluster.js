import React, { Component } from 'react';
import { Doughnut } from 'react-chartjs-2';

import './GaugeCluster.css';

const options = {
    rotation: 1 * Math.PI,
    circumference: 1 * Math.PI,
    responsive: true,
    legend: { display: false },
    tooltips: { enabled: false }
}

export default class GaugeCluster extends Component {

  render() {
    const cpuData = {
        labels: [
            "Cpu",
            "Remainder"
        ],
        datasets: [{
            data: [this.props.cpu, 100 - this.props.cpu],
            label: "cpu",
            backgroundColor: ["rgb(68, 0, 34)", "rgba(0,0,0,0.6)"],
            borderWidth: 2,
            borderColor: "rgba(0,0,0,1)",
            hoverBackgroundColor: ["rgb(68, 0, 34)", "rgba(0,0,0,0.6)"],
          }]
    }
    const memoryData = {
        labels: [
            "Memory",
            "Remainder"
        ],
        datasets: [{
            data: [this.props.memory, 100 - this.props.memory],
            label: "cpu",
            backgroundColor: ["rgb(68, 0, 34)", "rgba(0,0,0,0.6)"],
            borderWidth: 2,
            borderColor: "rgba(0,0,0,1)",
            hoverBackgroundColor: ["rgb(68, 0, 34)", "rgba(0,0,0,0.6)"],
          }]
    }
    return (
        <div className="gauges">
            <div className="gauge">
                <h3>CPU</h3>
                <Doughnut data={cpuData} options={options} />
            </div>
            <div className="gauge">
                <h3>MEMORY</h3>
                <Doughnut data={memoryData} options={options} />
            </div>
        </div>
    );
  }
}

