import React, { Component } from 'react';
import Client from '../Client/Client';
import './Clients.css';

export default class Clients extends Component {

  constructor() {
    super();
    this.state = {
      clients: []
    }
  }

  async componentDidMount() {
    const data = await this.getClientData();
    this.setState(state => { return { ...state, clients: data }});
    this.timer = setInterval(async () => {
      const data = await this.getClientData();
      this.setState(state => { return { ...state, clients: data}});
    }, 30000);
  }

  async getClientData() {
    try {
      const res = await fetch('http://localhost:3002');
      return res.json();
    } catch(e) {
      console.error(e);
    }
  }

  render() {
    return (
      <div className="clients">
        {this.state.clients.map(client => <Client key={client.identifier} client={client} />)}
      </div>
    );
  }
}
