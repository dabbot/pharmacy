import React, { Component } from 'react';
import Clients from '../Clients/Clients';
import GaugeCluster from '../GaugeCluster/GaugeCluster';

import './Home.css';

export default class Home extends Component {

  constructor() {
    super();
    this.state = {
      stats: {}
    }
  }

  async componentDidMount() {
    const data = await this.getClientData();
    this.setState(state => { return { ...state, stats: data }});
    this.timer = setInterval(async () => {
      const data = await this.getClientData();
      this.setState(state => { return { ...state, stats: data}});
    }, 5000);
  }

  async getClientData() {
    try {
      const res = await fetch('http://localhost:3003/stats');
      return res.json();
    } catch(e) {
      console.error(e);
    }
  }


  render() {
    return (
      <div className="home container">
        <div className="stats">
            <GaugeCluster cpu={this.state.stats.cpu} memory={this.state.stats.memory} />
        </div>
        <Clients />
      </div>
    );
  }
}
