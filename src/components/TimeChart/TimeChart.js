import React, { Component } from 'react';
import { Line } from 'react-chartjs-2';
import './TimeChart.css';

export default class TimeChart extends Component {
  
    render() {
      const times = this.props.times.map(t => t / 1000);
      const labels = Array.from(this.props.times.keys());
      const data = {
          labels: labels.map(x => x.toString()),
          
          datasets: [{
            gridLines: { display: false },
            lineTension: 0.4,
            data: times,
            label: "latency in μs",
            backgroundColor: 'rgba(0,0,0,0.1)'
          }]
      };

      const options = {
        scales: {
          xAxes: [
            {
              ticks: { display: false },
              gridLines: {display: false },
            }
          ],
          yAxes: [{ gridLines: { display: false } }]
        },
        responsive: true,
      };

      return (
        <div className="time-chart">
          <Line data={data} options={options}/>
        </div>
      );
  }
}

