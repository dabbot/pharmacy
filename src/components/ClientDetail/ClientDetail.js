import React, { Component } from 'react';
import TimeChart from '../TimeChart/TimeChart';

export default class ClientDetail extends Component {

  constructor() {
    super();
    this.state = {
      client: {}
    }
  }

  async componentDidMount() {
    const data = await this.getClientData();
    this.setState(state => { return { ...state, client: data.find(c => c.identifier === this.props.location.state.client.identifier) }});
    this.timer = setInterval(async () => {
      const data = await this.getClientData();
      this.setState(state => { return { ...state, client: data.find(c => c.identifier === state.client.identifier)}});
    }, 1000);
  }

  async getClientData() {
    try {
      const res = await fetch('http://localhost:3002');
      return res.json();
    } catch(e) {
      console.error(e);
    }
  }

  render() {
    if(!this.state.client.identifier) return (
      <div className="client client-detail">
      </div>
    );
    return (
      <div className="client client-detail">
        <h1>{this.state.client.identifier}</h1>
        <div className="client-detail-stats">
          <div className="client-detail-stats-messages">
            <h3>Past status messages</h3>
            <div className="client-detail-stats-messages-inner">
              {this.state.client.statusMessages.reverse().map(m => <span>{m}</span>)}
            </div>
          </div>
          <TimeChart times={this.state.client.times} />
        </div>
      </div>
    );
  }
}
